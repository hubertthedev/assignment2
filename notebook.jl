### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 58f95fc2-cd3f-11eb-18f7-2f341bb6738d
begin
	using Flux, Flux.Data.MNIST, Statistics
	using Flux.Data: DataLoader
	using Flux: onehotbatch, onecold, crossentropy, throttle
	using Flux
	using NNlib
	using Base.Iterators: repeated, partition
	using Printf, BSON
	using ImageView
	using Plots
	using Pkg
	using Images
	using ImageIO
	using FileIO
	using DataFrames
	using PlutoUI
	using Statistics
	using Plots
	using Base.Iterators: partition
	using Printf, BSON
	using Parameters: @with_kw
	using MLJFlux
	using ImageFeatures
	using ImageMagick
	using PyCall
	using TestImages
	using MLDatasets
	using MLDataUtils
	using ProgressMeter: @showprogress
	
	using Flux
using Flux.Data: DataLoader
using Flux.Optimise: Optimiser, WeightDecay
using Flux: onehotbatch, onecold
using Flux.Losses: logitcrossentropy
using Statistics, Random
using Logging: with_logger
using TensorBoardLogger: TBLogger, tb_overwrite, set_step!, set_step_increment!
using ProgressMeter: @showprogress
import BSON
using CUDA

end

# ╔═╡ 290f9364-6f44-45e5-83ff-7bb2daec4fdf
using CSV

# ╔═╡ d841f277-a7f6-467c-b311-ee68d651c8cb
md" # Assignment 2"

# ╔═╡ e5933ef6-4c3e-40fb-b791-0e978d38ac8a
md"> Predicting Presence of Pneumonia with Convolutional Neural Network"

# ╔═╡ 82df6682-40df-4042-acf8-f32958aaff9d
md" - Packages used in the program"

# ╔═╡ 5073fa48-e74f-4695-a552-a376b611bfee
md" - Path variable to the project folder"

# ╔═╡ 2b85e03a-b839-465d-8619-02f7c2656682
path = pwd()

# ╔═╡ ecb6a766-ab85-4efb-b0ad-a7ca46417373
md" ## args for functions"

# ╔═╡ fc08a576-6252-4de6-ae47-e6c6e01d6df2
Base.@kwdef mutable struct Args
    η = 3e-4             
    λ = 0                
    batchsize = 128      
    epochs = 10          
    seed = 0             
    use_cuda = true      
    infotime = 1 	     
    checktime = 5        
    tblogger = true      
    savepath = "results/"   
end

# ╔═╡ 8beacd9c-bd2e-4d5f-9636-f66ce75c9ac4
md" ## relu Activation Function"

# ╔═╡ 8be11cc6-fd1d-4e4e-958a-ed11db69d3dd
module Activations

export forward, ReLU

abstract type ReLU end

function forward(layer::Type{ReLU}, data)
    return map(x -> max(0, x), data)
end

end

# ╔═╡ df72a701-b88a-48ba-bd3c-403d3d83869b
begin
	xgrid = collect(range(-1, 1, length=100)) 
	plot(xgrid, NNlib.relu.(xgrid), label = "relu(x)", title="ReLU activation function", xlabel="x")
end

# ╔═╡ f619ac1c-74dc-441f-bcaa-deb928fef6ba
relu = relu(x) = max(0, x)

# ╔═╡ 7efa16d8-4975-4a4f-8479-5f7ceebbf02d


# ╔═╡ 1df95d81-8813-4e93-b006-6b6bfe7a9b0d
md" ## Loading Function"

# ╔═╡ 8b408c9b-983b-4519-aa95-1c16233dc6c5
function loading(paths)
	img = load(paths)
	img = Gray.(img)
	img = imresize(img,(32,32))
	img = vec(img)
	img = convert(Array{Float32,1},img)
	return img
	
end

# ╔═╡ 98c42e67-1c6b-46f7-aeed-70709b4a54bf
md" ## Normal TRAINING Set"

# ╔═╡ 6a201411-aa8a-4867-bcc7-dad0903275da
#Image Names
normal_train_images = readdir(path*"/chest_xray/train/NORMAL/")

# ╔═╡ fb17f515-85c1-4f9b-9d8d-3c43cab49901
#Image paths
normalTrainingPath = path*("/chest_xray/train/NORMAL/").*normal_train_images

# ╔═╡ 58bf2612-6864-4f47-b446-c930bf63e8cd
#Normal Image Training Set
normalTrainingSet = loading.(normalTrainingPath)

# ╔═╡ 9577c0a3-412c-4cca-9f1f-c5671c4125ba
Train_N = DataFrame(normalTrainingSet)

# ╔═╡ a08cdbbf-30a9-46f3-b3c0-202040599506
rows = size(Train_N)

# ╔═╡ e1bd8d14-f1e5-4d89-9a54-6735aeeb5a79


# ╔═╡ 4c6518d3-2556-4ce7-9e0d-828a3f1fd1ff
md" ## Normal TESTING Set"

# ╔═╡ 691dab92-c277-474a-ab88-494732c57a3b
#Image Names
normal_test_images = readdir(path*"/chest_xray/test/NORMAL/")

# ╔═╡ ad84e765-f47b-403b-8582-e94ac0c58d20
#Image Paths
normalTestingPath = path*("/chest_xray/test/NORMAL/").*normal_test_images

# ╔═╡ 8ca47c22-0e5e-4839-95ef-f7fe9bd29ce0
#Normal Image Testing Set
normalTestingSet = loading.(normalTestingPath)

# ╔═╡ 735e9a51-ff7b-4a4a-b761-c74a1e1031d3
Test_N = DataFrame(normalTestingSet)

# ╔═╡ fe2a58da-3a6d-420e-a90c-d42d831da23f
rows1 = size(Test_N)

# ╔═╡ bb4659c1-e1ed-47af-88da-23ee3ba3ad68


# ╔═╡ df51b281-e15a-4b4e-8ebb-b4aa4a68f1f6
md" ## Pneumonia TRAINING Set"

# ╔═╡ e9783db6-39e9-442c-9dbd-970ec681770d
#Training Image Names
pneumonia_train_images = readdir(path*"/chest_xray/train/PNEUMONIA/")

# ╔═╡ c86546b1-47cb-486e-a9d5-188317304f26
#Training Paths

pneumoniaTrainingPath = path*("/chest_xray/train/PNEUMONIA/").*pneumonia_train_images

# ╔═╡ cce0d0af-d8dc-447e-aa08-83a120bf37d8
#Pneumonia Training Set
pneumoniaTrainingSet = loading.(pneumoniaTrainingPath)

# ╔═╡ 897bd95a-2201-46b3-b992-511ad0dfa019
P_Train = DataFrame(pneumoniaTrainingSet)

# ╔═╡ c2303fe7-a7de-4b3f-83e5-c677ee8cab6a
rows2 = size(P_Train)

# ╔═╡ 168d43e0-4b7f-4820-9181-f4dc13242beb


# ╔═╡ 38d8c715-d443-417c-896d-cb256085a094
md" ## Pneumonia TESTING set"

# ╔═╡ 67ff03eb-4b58-4284-a1d6-c9758f6f4696
#Image names
pneumonia_test_images = readdir(path*"/chest_xray/test/PNEUMONIA/")

# ╔═╡ 78eb6193-7bc4-4f49-b38b-008ad4edefc6
#Testing Paths
pneumoniaTestingPath = path*("/chest_xray/test/PNEUMONIA/").*pneumonia_test_images

# ╔═╡ bc574290-e040-45ff-bc1d-4a46b73bbe56
#Pneumonia Training Set
pneumoniaTestingSet = loading.(pneumoniaTestingPath)

# ╔═╡ 9eea2de0-d96a-4d99-b960-8590ec142c3b
P_Test = DataFrame(pneumoniaTestingSet)

# ╔═╡ 693c6b85-d65b-419b-a8a7-9cfcefba291c
rows3 = size(P_Test)

# ╔═╡ 2915d988-10b6-4a0c-b4b2-b05117d34c91
md" ## Normal Sets"

# ╔═╡ e6974f51-a512-49dd-8ea4-ba4943ecbf8a

normal=vcat(normalTrainingSet, normalTestingSet) 


# ╔═╡ f9edd971-6d37-495d-9625-3b2de66e724d


# ╔═╡ de9b9897-dd4a-4909-bd95-0c437e0fa99b
md" ## Pneumonia Sets"

# ╔═╡ 2f36fd0f-dec1-4788-afa1-772daf3a6392
pneumonia = vcat(pneumoniaTrainingSet,pneumoniaTestingSet)

# ╔═╡ e4530131-19d4-4faf-97b8-77a6b0bbee2b
data = vcat(normal,pneumonia)

# ╔═╡ a6041b6b-f1f7-4901-82b7-9674fe351e0e
  labels = vcat([0 for _ in 1:length(normal)], [1 for _ in 1:length(pneumonia)])


# ╔═╡ 248263ca-339e-4a8a-8899-d4b72c151bbb
(x_train, y_train), (x_test, y_test) = splitobs(shuffleobs((data, labels)), at = 0.7)

# ╔═╡ e2b5ddee-7c60-454a-810b-40bdf7cd435d
xtrain = DataFrame(x_train)

# ╔═╡ d1c412ef-eb40-4873-9793-8442baafdaf0
CSV.write(path*"/xtrain.csv", xtrain)

# ╔═╡ b5c7af91-064e-4d64-a66d-50d579b343f7
xtest = DataFrame(x_test)

# ╔═╡ 91441b6b-623b-445e-b934-c2e0ead1fab5
CSV.write(path*"/xtest.csv",xtest)

# ╔═╡ cccf3647-fdc4-4311-bf24-9e336b36cf5f
md" ## batch Function"

# ╔═╡ b5ea331c-dcb5-4261-a74f-bcb778a80308
begin
	
	 train_loader = DataLoader((x_train, y_train), batchsize=128, shuffle=true)
	
	    test_loader = DataLoader((x_test, y_test),  batchsize=128)
end

# ╔═╡ b67980f7-f099-4111-bb06-f14e8a8be9f5
function Data(args)
	(x_train, y_train), (x_test, y_test) = splitobs(shuffleobs((data, labels)), at = 0.7)

    x_train = reshape(x_train, 28, 28, 1, :)
    x_test = reshape(x_test, 28, 28, 1, :)

    y_train, y_test = onehotbatch(y_train, 0:9), onehotbatch(ytest, 0:9)

    train_loader = DataLoader((x_train, y_train), batchsize=args.batchsize, shuffle=true)
    test_loader = DataLoader((x_test, y_test),  batchsize=args.batchsize)
    
    return train_loader, test_loader
end

# ╔═╡ b8ccc9ec-9832-4b27-b8b9-502e5de25456
md" ## utility Function"

# ╔═╡ 21e9dabc-0142-47de-96a8-12fb45353b4c
begin
	num_params(model) = sum(length, Flux.params(model)) 
	round4(x) = round(x, digits=4)
end

# ╔═╡ afafab83-b16f-408b-a7f1-8d8607d6d164
md" ## Convolutional Neural Network Model"

# ╔═╡ 09c00362-f7a9-43a3-9a44-7b92f9958e51
md" >In Lenet-5

>LeNet-5, the most popular LeNet people talked about, only has slight differences >compared with LeNet-4.
- 32×32 input image >
- Six 28×28 feature maps convolutional layer (5×5 size) >
- Average Pooling layers (2×2 size) >
- Sixteen 10×10 feature maps convolutional layer (5×5 size) >
- Average Pooling layers (2×2 size) >
- Fully connected to 120 neurons >
- Fully connected to 84 neurons >
- Fully connected to 10 outputs"

# ╔═╡ 48f430b3-aaa8-4f96-b35a-9d093703443b
function LeNet5(; imgsize=(28,28,1), nclasses=2) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
            Conv((5, 5), imgsize[end]=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            flatten,
            Dense(prod(out_conv_size), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, nclasses)
          )
end

# ╔═╡ 90759dcc-3107-48e1-b902-c4bece6a3738
md" ## Loss Function"

# ╔═╡ 51c6389a-6a50-4130-8f75-83e03123a664
begin
	function loss(x, y)
	    x_aug = x .+ 0.1f0*gpu(randn(eltype(x), size(x)))
	
	    y_hat = model(x_aug)
	    return crossentropy(y_hat, y)
	end
	accuracy(x, y) = mean(onecold(model(x)) .== onecold(y))
	
	opt = ADAM(0.001);
end

# ╔═╡ 2cf8ddc1-83b4-4f04-8ff4-4f75877061c9
function lossAccuracy(loader, model, device)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        x, y = x |> device, y |> device
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ |> cpu) .== onecold(y |> cpu))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end

# ╔═╡ c62dd701-cb4e-4cb0-bc0b-cbe61f789f86
md" ## Training Function"

# ╔═╡ 59aa1865-c1d7-44bc-999e-0f2c6041c89f
function train(; kws...)
    args = Args(; kws...)
    args.seed > 0 && Random.seed!(args.seed)
    use_cuda = args.use_cuda && CUDA.functional()
    
    if use_cuda
        device = gpu
        @info "Training on GPU"
    else
        device = cpu
        @info "Training on CPU"
    end

    
    train_loader, test_loader = Data(args)
    @info "Dataset MNIST: $(train_loader.nobs) train and $(test_loader.nobs) test examples"

    model = LeNet5() |> device
    @info "LeNet5 model: $(num_params(model)) trainable params"    
    
    ps = Flux.params(model)  

    opt = ADAM(args.η) 
    if args.λ > 0 
        opt = Optimiser(WeightDecay(args.λ), opt)
    end
    
    if args.tblogger 
        tblogger = TBLogger(args.savepath, tb_overwrite)
        set_step_increment!(tblogger, 0) 
        @info "TensorBoard logging at \"$(args.savepath)\""
    end
function report(epoch)
        train = eval_loss_accuracy(train_loader, model, device)
        test = eval_loss_accuracy(test_loader, model, device)        
        println("Epoch: $epoch   Train: $(train)   Test: $(test)")
        if args.tblogger
            set_step!(tblogger, epoch)
            with_logger(tblogger) do
                @info "train" loss=train.loss  acc=train.acc
                @info "test"  loss=test.loss   acc=test.acc
            end
        end
    end
@info "Start Training"
    report(0)
    for epoch in 1:args.epochs
        @showprogress for (x, y) in train_loader
            x, y = x |> device, y |> device
            gs = Flux.gradient(ps) do
                    ŷ = model(x)
                    loss(ŷ, y)
                end

            Flux.Optimise.update!(opt, ps, gs)
        end
        
        epoch % args.infotime == 0 && report(epoch)
        if args.checktime > 0 && epoch % args.checktime == 0
            !ispath(args.savepath) && mkpath(args.savepath)
            modelpath = joinpath(args.savepath, "model.bson") 
            let model = cpu(model) 
                BSON.@save modelpath model epoch
            end
            @info "Model saved in \"$(modelpath)\""
        end
    end
end

# ╔═╡ cd7bb2de-5fa6-45f4-8eec-afa7c9bf55cf
md" ## training"

# ╔═╡ ba2ba1ce-4d67-4251-93da-4569eff8781b
train()

# ╔═╡ b8abf00f-c332-4f72-9d97-9e7d1d722d2e


# ╔═╡ Cell order:
# ╟─d841f277-a7f6-467c-b311-ee68d651c8cb
# ╟─e5933ef6-4c3e-40fb-b791-0e978d38ac8a
# ╟─82df6682-40df-4042-acf8-f32958aaff9d
# ╠═58f95fc2-cd3f-11eb-18f7-2f341bb6738d
# ╟─5073fa48-e74f-4695-a552-a376b611bfee
# ╟─2b85e03a-b839-465d-8619-02f7c2656682
# ╟─ecb6a766-ab85-4efb-b0ad-a7ca46417373
# ╠═fc08a576-6252-4de6-ae47-e6c6e01d6df2
# ╟─8beacd9c-bd2e-4d5f-9636-f66ce75c9ac4
# ╠═8be11cc6-fd1d-4e4e-958a-ed11db69d3dd
# ╠═df72a701-b88a-48ba-bd3c-403d3d83869b
# ╠═f619ac1c-74dc-441f-bcaa-deb928fef6ba
# ╠═7efa16d8-4975-4a4f-8479-5f7ceebbf02d
# ╟─1df95d81-8813-4e93-b006-6b6bfe7a9b0d
# ╠═8b408c9b-983b-4519-aa95-1c16233dc6c5
# ╟─98c42e67-1c6b-46f7-aeed-70709b4a54bf
# ╠═6a201411-aa8a-4867-bcc7-dad0903275da
# ╠═fb17f515-85c1-4f9b-9d8d-3c43cab49901
# ╠═58bf2612-6864-4f47-b446-c930bf63e8cd
# ╠═9577c0a3-412c-4cca-9f1f-c5671c4125ba
# ╠═a08cdbbf-30a9-46f3-b3c0-202040599506
# ╠═e1bd8d14-f1e5-4d89-9a54-6735aeeb5a79
# ╟─4c6518d3-2556-4ce7-9e0d-828a3f1fd1ff
# ╠═691dab92-c277-474a-ab88-494732c57a3b
# ╠═ad84e765-f47b-403b-8582-e94ac0c58d20
# ╠═8ca47c22-0e5e-4839-95ef-f7fe9bd29ce0
# ╠═735e9a51-ff7b-4a4a-b761-c74a1e1031d3
# ╠═fe2a58da-3a6d-420e-a90c-d42d831da23f
# ╠═bb4659c1-e1ed-47af-88da-23ee3ba3ad68
# ╟─df51b281-e15a-4b4e-8ebb-b4aa4a68f1f6
# ╠═e9783db6-39e9-442c-9dbd-970ec681770d
# ╠═c86546b1-47cb-486e-a9d5-188317304f26
# ╠═cce0d0af-d8dc-447e-aa08-83a120bf37d8
# ╠═897bd95a-2201-46b3-b992-511ad0dfa019
# ╠═c2303fe7-a7de-4b3f-83e5-c677ee8cab6a
# ╠═168d43e0-4b7f-4820-9181-f4dc13242beb
# ╟─38d8c715-d443-417c-896d-cb256085a094
# ╠═67ff03eb-4b58-4284-a1d6-c9758f6f4696
# ╠═78eb6193-7bc4-4f49-b38b-008ad4edefc6
# ╠═bc574290-e040-45ff-bc1d-4a46b73bbe56
# ╠═9eea2de0-d96a-4d99-b960-8590ec142c3b
# ╠═693c6b85-d65b-419b-a8a7-9cfcefba291c
# ╟─2915d988-10b6-4a0c-b4b2-b05117d34c91
# ╠═e6974f51-a512-49dd-8ea4-ba4943ecbf8a
# ╠═f9edd971-6d37-495d-9625-3b2de66e724d
# ╟─de9b9897-dd4a-4909-bd95-0c437e0fa99b
# ╠═2f36fd0f-dec1-4788-afa1-772daf3a6392
# ╠═e4530131-19d4-4faf-97b8-77a6b0bbee2b
# ╠═a6041b6b-f1f7-4901-82b7-9674fe351e0e
# ╠═248263ca-339e-4a8a-8899-d4b72c151bbb
# ╠═290f9364-6f44-45e5-83ff-7bb2daec4fdf
# ╠═e2b5ddee-7c60-454a-810b-40bdf7cd435d
# ╠═d1c412ef-eb40-4873-9793-8442baafdaf0
# ╠═b5c7af91-064e-4d64-a66d-50d579b343f7
# ╠═91441b6b-623b-445e-b934-c2e0ead1fab5
# ╟─cccf3647-fdc4-4311-bf24-9e336b36cf5f
# ╠═b5ea331c-dcb5-4261-a74f-bcb778a80308
# ╠═b67980f7-f099-4111-bb06-f14e8a8be9f5
# ╟─b8ccc9ec-9832-4b27-b8b9-502e5de25456
# ╠═21e9dabc-0142-47de-96a8-12fb45353b4c
# ╟─afafab83-b16f-408b-a7f1-8d8607d6d164
# ╟─09c00362-f7a9-43a3-9a44-7b92f9958e51
# ╠═48f430b3-aaa8-4f96-b35a-9d093703443b
# ╟─90759dcc-3107-48e1-b902-c4bece6a3738
# ╠═51c6389a-6a50-4130-8f75-83e03123a664
# ╠═2cf8ddc1-83b4-4f04-8ff4-4f75877061c9
# ╟─c62dd701-cb4e-4cb0-bc0b-cbe61f789f86
# ╠═59aa1865-c1d7-44bc-999e-0f2c6041c89f
# ╟─cd7bb2de-5fa6-45f4-8eec-afa7c9bf55cf
# ╠═ba2ba1ce-4d67-4251-93da-4569eff8781b
# ╠═b8abf00f-c332-4f72-9d97-9e7d1d722d2e
